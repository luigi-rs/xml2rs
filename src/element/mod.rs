/* ELEMENT				MAIN LENGTH PROPORTIONAL TO CROSS LENGTH
 * same-axis container	not if len
 * diff-axis container	not if len
 * radio btn			yes
 * checkbox				yes
 * slider				yes
 * number				yes
 * date					yes
 * text					yes
 * label				yes
 * image				yes
 * media				yes
 * link					yes
 * guide				[no but not rendered rn]
 */

use std::fmt;

mod input;
mod container;
mod use_component;
mod gen_component;
mod text;
mod label;
mod image;
mod media;
mod link;
mod guide;

pub use container::Container;
pub use input::Input;

pub mod attributes;
pub use attributes::Attr;
pub use attributes::AttrVariant;
pub use attributes::AttrValue;
pub use attributes::AttrList;

#[derive(Debug, Clone, Copy)]
pub enum Grow {
	No,
	Before,
	Around,
	After,
}

impl Grow {
	pub fn parse(value: &str) -> Option<Grow> {
		match value {
			"around" => Some(Grow::Around),
			"after" => Some(Grow::After),
			"before" => Some(Grow::Before),
			_ => None?,
		}
	}
}

#[derive(Debug, Clone)]
pub enum Component {
	Use(String),
	Gen(String),
}

#[derive(Clone)]
pub enum Element {
	Component(Component),
	Container(Container),
	Input(Input),
	Text,
	Label,
	Image,
	Media,
	Link,
	Guide,
}

impl Element {
	pub fn parse(prefix: &str, name: &str) -> Option<Self> {
		match prefix {
			"use" => Some(Self::Component(Component::Use(name.into()))),
			"gen" => Some(Self::Component(Component::Gen(name.into()))),
			"" => {
				match name {
					"x"     => Some(Self::Container(Container::X)),
					"y"     => Some(Self::Container(Container::Y)),
					"text"  => Some(Self::Text),
					"label" => Some(Self::Label),
					"image" => Some(Self::Image),
					"media" => Some(Self::Media),
					"link"  => Some(Self::Link),
					"guide" => Some(Self::Guide),
					_ => Some(Self::Input(Input::parse(name)?)),
				}
			},
			_ => None,
		}
	}

	pub fn attrs(&self) -> (&[AttrVariant], &[AttrVariant]) {
		match self {
			Self::Component(component) => match component {
				Component::Use(_) => (&use_component::REQUIRES, &use_component::SUPPORTS),
				Component::Gen(_) => (&gen_component::REQUIRES, &gen_component::SUPPORTS),
			},
			Self::Container(_) => (&container::REQUIRES, &container::SUPPORTS),
			Self::Text         => (     &text::REQUIRES,      &text::SUPPORTS),
			Self::Label        => (    &label::REQUIRES,     &label::SUPPORTS),
			Self::Image        => (    &image::REQUIRES,     &image::SUPPORTS),
			Self::Media        => (    &media::REQUIRES,     &media::SUPPORTS),
			Self::Link         => (     &link::REQUIRES,      &link::SUPPORTS),
			Self::Guide        => (    &guide::REQUIRES,     &guide::SUPPORTS),
			Self::Input(input) => input.attrs(),
		}
	}

	pub fn check_attrs(&self, attrs: &AttrList) -> (Vec<&'static str>, Vec<&'static str>) {
		let mut missing = Vec::new();
		let mut invalid = Vec::new();
		let (requires, supports) = self.attrs();
		let r_len = requires.len();
		let s_len = supports.len();
		let mut present = requires
			.iter().map(|_| false)
			.collect::<Vec<bool>>();
		attrs.foreach(|attr| {
			let variant = attr.variant();
			for i in 0..r_len {
				if requires[i] == variant {
					present[i] = true;
					return;
				}
			}
			for i in 0..s_len {
				if supports[i] == variant {
					return;
				}
			}
			invalid.push(attr.variant().as_xml_attr());
		});
		for i in 0..r_len {
			if !present[i] {
				missing.push(requires[i].as_xml_attr());
			}
		}
		(missing, invalid)
	}
}

impl fmt::Debug for Element {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Self::Component(component) => write!(f, "{:?}", component),
			Self::Container(container) => write!(f, "{:?}", container),
			Self::Input(input)         => write!(f, "{:?}", input),
			Self::Text  => write!(f, "Text"),
			Self::Label => write!(f, "Label"),
			Self::Image => write!(f, "Image"),
			Self::Media => write!(f, "Media"),
			Self::Link  => write!(f, "Link"),
			Self::Guide => write!(f, "Guide"),
		}
	}
}

impl fmt::Display for Grow {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", match self {
			Grow::No => "GROW_NO",
			Grow::Before => "GROW_BEFORE",
			Grow::Around => "GROW_AROUND",
			Grow::After => "GROW_AFTER",
		})
	}
}
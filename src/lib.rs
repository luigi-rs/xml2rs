use std::collections::HashSet;

pub mod identifier;
pub mod units;
pub mod element;
pub mod tree;

use tree::Tree;
use element::Element;
use element::Component::Use;
use element::Component::Gen;
use element::Container;

fn s(s: &str) -> String {
	String::from(s)
}

fn mod_code(tree: &Tree) -> String {
	let mut code = String::new();
	let mut comps = HashSet::new();
	for i in tree.renderables() {
		let (e, _, _) = tree.get(i);
		let comp = match e {
			Element::Component(Use(comp)) |
			Element::Component(Gen(comp)) => comp,
			_ => continue,
		};
		if comps.contains(comp) {
			continue;
		}
		code += "#[path = \"";
		code += comp;
		code += ".rs\"]\n";
		code += "mod ";
		code += comp;
		code += ";\n";
		comps.insert(comp);
	}
	code
}

fn struct_code(tree: &Tree) -> String {
	let mut code = String::from("{");
	let mut need_lifetime = false;
	for i in tree.renderables() {
		let (_, attrs, _) = tree.get(i);
		let (attrs_code, need_lifetime_2) = attrs.to_declaration();
		code += &attrs_code;
		if need_lifetime_2 && !need_lifetime {
			need_lifetime = true;
		}
	}
	if need_lifetime {
		code.insert_str(0, "<'a>");
	}
	code + "}"
}

// recursive
fn len_code(tree: &Tree, i: usize, c_idx: Option<usize>, parent: Container) -> (String, bool) {
	let (e, attrs, d) = tree.get(i);
	let mut code = String::new();
	let mut need_rem = false;
	if let Element::Container(c) = e {
		need_rem = c.needs_rem(&attrs);
		code += &c.before_children(&attrs, parent, c_idx);
		let mut rem_cc = String::new();
		let mut rem_c_count = 0;
		{
			let mut c_idx = 0;
			for i in tree.children(i) {
				let (cc, need_rem) = len_code(tree, i, Some(c_idx), *c);
				if need_rem {
					rem_cc += &cc;
					rem_c_count += 1;
				} else {
					code += &cc;
				}
				c_idx += 1;
			}
		}
		if rem_c_count > 0 {
			code += &c.children_sum();
			code += &c.rem_unit(rem_c_count);
			code += &rem_cc;
		}
		if c.needs_children_sum(&attrs, parent) {
			code += &c.children_sum();
		}
		code += &c.after_children(&attrs, parent, c_idx);
	} else {
		let (prefix, suffix) = match d {
			0 => ("", ".1"),
			_ => ("inner.push(", ");"),
		};
		let attrs_struct = attrs.to_instantiation();
		let (function, args) = match e {
			Element::Component(Use(c)) => {
				let mut function = String::from(c);
				function += "::len";
				let mut args = String::from("ctx,&");
				args += &c;
				args += "::Instance";
				args += attrs_struct.as_str();
				args += ",cross,_main";
				(function, args)
			},
			Element::Label => (s("ctx.text.label"), s("LabelAttrs") + attrs_struct.as_str() + ",cross"),
			Element::Link => (s("ctx.text.text"), s("TextAttrs") + attrs_struct.as_str() + ",cross"),
			_ => (s("element"), s("0,cross")),
		};
		code += &format!("{}({}, {}_{}({})){}",
			prefix,
			c_idx.unwrap(),
			&function,
			parent.letter(),
			&args,
			suffix);
	}
	(code, need_rem)
}

// recursive
fn rendering_code(tree: &Tree, i: usize, c_idx: Option<usize>, parent: Container) -> String {
	let (e, attrs, _) = tree.get(i);
	
	let ulen = match c_idx {
		None => String::from("main"),
		Some(c_idx) => format!("get_c(&len, {})", c_idx),
	};

	let mut code = String::new();
	match e {
		Element::Container(c) => {
			code += "{let (mut left, mut top) = (left, top);";
			/*let (pre, post) = match parent {
				Container::X => ("", ", _cross"),
				Container::Y => ("_cross, ", ""),
			};
			code += &format!("element_render(0,left,top,{}{}.px{});", pre, ulen, post);*/
			if parent != *c {
				code += &format!("let _cross = {}.px;", ulen);
			}
			code += &format!("let len = &{}.inner.as_ref().unwrap().0;", ulen);
			{
				let mut c_idx = 0;
				for i in tree.children(i) {
					code += &rendering_code(tree, i, Some(c_idx), *c);
					c_idx += 1;
				}
			}
			code += "}";
		},
		Element::Component(Use(c)) => {
			let attrs_struct = attrs.to_instantiation();
			code += &format!("{}::render_{}(ctx,&{}::Instance{},left,top,{},_cross);",
				&c,
				parent.letter(),
				&c,
				&attrs_struct.as_str(),
				&ulen);
		},
		_ => {
			let attrs_struct = attrs.to_instantiation();
			let (function, arg_type, arg) = match e {
				Element::Label => ("ctx.text.label_queue", "LabelAttrs", attrs_struct.as_str()),
				Element::Link => ("ctx.text.text_queue", "TextAttrs", attrs_struct.as_str()),
				_ => ("element_render", "", "0"),
			};
			let (pre, post) = match parent {
				Container::X => ("", ", _cross"),
				Container::Y => ("_cross, ", ""),
			};
			code += &format!("{}({}{},left,top,{}{}.px{});",
				function, arg_type, arg,
				pre, ulen, post);
		},
	}
	code += &format!("{} += {}.px;", match parent {
		Container::X => "left",
		Container::Y => "top",
	}, ulen);
	code
}

pub fn convert(xml: &str) -> String {
	let mut tree = Tree::new();
	let mut code = String::new();
	if let Err(msg) = tree.add_from_xml(xml) {
		code += &format!("/* xml2rs encountered an issue:\n{}\n*/", msg);
	} else {
		let top_cont = Container::Y;
		code += "use luigi::*;\n";
		code += &mod_code(&tree);
		code += "\n\n";

		code += "#[allow(non_camel_case_types)]\n#[derive(Default)]\n";
		code += "pub struct Instance";
		code += &struct_code(&tree);
		code += "\n\n";

		code += "#[allow(unused_mut, unused_variables)]\n";
		code += &format!("pub fn len_{}", top_cont.letter());
		code += "(ctx: &mut Context, instance: &Instance, cross: usize, _main: usize) -> UsedLength {";
		code += &len_code(&tree, tree.root().unwrap(), None, top_cont).0;
		code += "}\n\n";

		code += "#[allow(unused_mut, unused_assignments, unused_variables)]\n";
		code += &format!("pub fn render_{}", top_cont.letter());
		code += "(ctx: &mut Context, instance: &Instance, mut left: usize, mut top: usize, main: &UsedLength, _cross: usize) {";
		code += &rendering_code(&tree, tree.root().unwrap(), None, top_cont);
		code += "}\n";
	}
	code
}

use crate::element::AttrVariant;

pub const SUPPORTS: [AttrVariant; 0] = [];

pub const REQUIRES: [AttrVariant; 1] = [
	AttrVariant::Factory,
];
use std::fmt;

#[derive(Debug, Clone)]
pub struct Identifier(String);

impl Identifier {
	pub fn parse(value: &str) -> Option<Self> {
		let mut first = true;
		for c in value.chars() {
			if first {
				if c.is_alphabetic() {
					first = false;
					continue;
				} else {
					return None;
				}
			} else if c.is_alphanumeric() || c == '_' {
				continue;
			} else {
				return None;
			}
		}
		Some(Self(value.into()))
	}
}

#[derive(Debug, Clone)]
pub struct Callback(Identifier);

impl Callback {
	pub fn parse(value: &str) -> Option<Self> {
		Some(Self(Identifier::parse(value)?))
	}
}

impl fmt::Display for Identifier {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.0)
	}
}

impl fmt::Display for Callback {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.0)
	}
}

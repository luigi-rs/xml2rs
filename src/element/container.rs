use crate::element::AttrVariant as V;
use crate::element::AttrList;
use crate::element::attributes::AttrValue;
use crate::element::attributes::ActualAttr;
use crate::element::attributes::Attr;
use crate::element::Grow;
use crate::units::Length;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Container {
	X,
	Y,
}

pub const SUPPORTS: [V; 9] = [
	V::Grow,
	V::Pad,
	V::Box,
	V::Style,
	V::Hover,
	V::Length,
	V::Divider,
	V::Overrun,
	V::Overflow,
];

pub const REQUIRES: [V; 0] = [];

impl Container {
	pub fn needs_rem(self, attrs: &AttrList) -> bool {
		if let Some(Attr::ActualAttr(ActualAttr::Length(AttrValue::Constant(Length::Remaining)))) = attrs.get(V::Length) {
			true
		} else {
			false
		}
	}

	pub fn before_children(self, attrs: &AttrList, parent: Self, i_as_child: Option<usize>) -> String {
		let mut code = match i_as_child {
			Some(i) => format!("inner.push(({}, {{", i),
			None => String::new(),
		};
		if self != parent {
			code += &format!("let _main = cross;");
			code += if !attrs.has(V::Length) {
				"let mut "
			} else {
				"let "
			};
			code += &format!("cross = {};", attrs.code(V::Length, 0));
			if !attrs.has(V::Length) {
				code += "loop {";
			}
		}
		code += "let mut inner = Vec::new();";
		code
	}

	pub fn needs_children_sum(self, attrs: &AttrList, parent: Self) -> bool {
		!attrs.has(V::Length) && self == parent
	}

	pub fn children_sum(self) -> String {
		String::from("let children_sum = inner.iter().fold(0, len_acc);")
	}

	pub fn rem_unit(self, rem_children_count: usize) -> String {
		format!("let rem_unit = (_main - children_sum) / {};", rem_children_count)
	}

	pub fn after_children(self, attrs: &AttrList, parent: Self, i_as_child: Option<usize>) -> String {
		let mut code = String::new();
		if attrs.has(V::Length) {
			if self == parent {
				code += &format!("let main = {};", attrs.code(V::Length, 0));
			}
		} else {
			if self == parent {
				code += "let main = children_sum;";
			} else {
				code += "if cross == 0 { ";
				code += &format!("cross = compute_cross(&inner);");
				code += " if cross != 0 { continue; } ";
				code += "} break ";
			}
		}
		code += "UsedLength {";
		code += &format!("inner: Some((inner, {})),", parent == self);
		code += &format!("grow: {},", attrs.code(V::Grow, Grow::No));
		code += &format!("px: {},", match self != parent {
			false => "main",
			true => "cross",
		});
		code += if self == parent || attrs.has(V::Length) {
			"}"
		} else {
			"}}"
		};
		if let Some(_) = i_as_child {
			code += "}));";
		}
		code
	}

	pub fn letter(self) -> char {
		match self {
			Container::X => 'h',
			Container::Y => 'v',
		}
	}
}
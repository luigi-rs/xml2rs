use crate::element::AttrVariant;

pub const SUPPORTS: [AttrVariant; 5] = [
	AttrVariant::Grow,
	AttrVariant::Pad,
	AttrVariant::Box,
	AttrVariant::Style,
	AttrVariant::Hover,
];

pub const REQUIRES: [AttrVariant; 4] = [
	AttrVariant::Name,
	AttrVariant::Start,
	AttrVariant::Stop,
	AttrVariant::Step,
];
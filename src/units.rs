use std::fmt;
use crate::identifier::Callback;

#[derive(Debug, Clone, Copy)]
pub enum LengthUnit {
	Pixel,   // px
	Font,    // ch
}

#[derive(Debug, Clone)]
pub struct DefinedLength {
	pub repr: String,
	pub unit: LengthUnit,
}

#[derive(Debug, Clone)]
pub enum Length {
	Constant(DefinedLength),
	Variable(Callback),
	Remaining,
}

fn parse_constant(value: &str) -> Option<Length> {
	let mut unit_idx = 0;
	for c in value.chars() {
		if c.is_digit(10) || c == '.' {
			unit_idx += 1;
		} else {
			break;
		}
	}
	let (value, unit) = value.split_at(unit_idx);
	value.parse::<f64>().ok()?;
	let repr = String::from(value);
	let unit = match unit {
		"px" => LengthUnit::Pixel,
		"ch" => LengthUnit::Font,
		_ => None?,
	};
	Some(Length::Constant(DefinedLength { repr, unit }))
}

fn parse_variable(value: &str) -> Option<Length> {
	Some(Length::Variable(Callback::parse(value)?))
}

impl Length {
	pub fn parse(value: &str) -> Option<Length> {
		if value == "left" {
			Some(Length::Remaining)
		} else {
			match value.chars().next() {
				Some(c) if c.is_digit(10) => parse_constant(value),
				Some(c) if c.is_alphabetic() => parse_variable(value),
				_ => None,
			}
		}
	}
}

impl fmt::Display for Length {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Self::Constant(deflen) => {
				write!(f, "{}{}", deflen.repr, match deflen.unit {
					LengthUnit::Pixel => "",
					LengthUnit::Font => " * style.font.ch"
				})
			},
			Self::Variable(var) => write!(f, "callbacks.{}()", var),
			Self::Remaining => write!(f, "rem_unit"),
			// Self::Remaining => write!(f, "/* invalid use of `left` */"),
		}
	}
}
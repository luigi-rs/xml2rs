use crate::element::AttrVariant;

pub const SUPPORTS: [AttrVariant; 21] = [
	AttrVariant::Factory,
	AttrVariant::Grow,
	AttrVariant::Pad,
	AttrVariant::Box,
	AttrVariant::Style,
	AttrVariant::Hover,
	AttrVariant::Length,
	AttrVariant::Divider,
	AttrVariant::Overrun,
	AttrVariant::Overflow,
	AttrVariant::Content,
	AttrVariant::Source,
	AttrVariant::Destination,
	AttrVariant::Name,
	AttrVariant::Start,
	AttrVariant::Stop,
	AttrVariant::Step,
	AttrVariant::Channel,
	AttrVariant::Min,
	AttrVariant::Max,
	AttrVariant::Grain,
];

pub const REQUIRES: [AttrVariant; 0] = [];

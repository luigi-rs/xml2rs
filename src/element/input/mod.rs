use crate::element::AttrVariant;

mod radio;
mod checkbox;
mod slider;
mod number;
mod date;

#[derive(Debug, Clone)]
pub enum Input {
	Radio,
	Checkbox,
	Slider,
	Number,
	Date,
}

impl Input {
	pub fn parse(name: &str) -> Option<Self> {
		match name {
			"radio"    => Some(Self::Radio),
			"checkbox" => Some(Self::Checkbox),
			"slider"   => Some(Self::Slider),
			"number"   => Some(Self::Number),
			"date"     => Some(Self::Date),
			_ => None,
		}
	}

	pub fn attrs(&self) -> (&[AttrVariant], &[AttrVariant]) {
		match self {
			Self::Radio    => (   &radio::REQUIRES,    &radio::SUPPORTS),
			Self::Checkbox => (&checkbox::REQUIRES, &checkbox::SUPPORTS),
			Self::Slider   => (  &slider::REQUIRES,   &slider::SUPPORTS),
			Self::Number   => (  &number::REQUIRES,   &number::SUPPORTS),
			Self::Date     => (    &date::REQUIRES,     &date::SUPPORTS),
		}
	}
}

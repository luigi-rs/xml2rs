use std::fmt;
use crate::units::Length;
use crate::identifier::Identifier;
use crate::identifier::Callback;
use super::Grow;

#[derive(Debug, Clone)]
pub enum AttrValue<T> {
	Constant(T),
	Variable(String),
}

use AttrValue::Constant;
use AttrValue::Variable;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum AttrVariant {
	Default,
	Factory,
	Grow,
	Pad,
	Box,
	Style,
	Hover,
	Length,
	Divider,
	Overrun,
	Overflow,
	Content,
	Source,
	Destination,
	Name,
	Start,
	Stop,
	Step,
	Channel,
	Min,
	Max,
	Grain,
}

#[derive(Clone, Debug)]
pub enum ActualAttr {
	// default values
	Default,
	// component gen
	Factory    (AttrValue<Callback>),
	// visible
	Grow       (AttrValue<Grow>),
	Pad        (AttrValue<Length>),
	Box        (AttrValue<Length>),
	Style      (AttrValue<Identifier>),
	Hover      (AttrValue<Identifier>),
	// container
	Length     (AttrValue<Length>),
	Divider    (AttrValue<Length>),
	Overrun    (AttrValue<Callback>),
	Overflow   (AttrValue<Callback>),
	// text, label & link
	Content    (AttrValue<String>),
	// image & media
	Source     (AttrValue<String>),
	// link
	Destination(AttrValue<String>),
	// checkbox, date
	Name       (AttrValue<Identifier>),
	// date
	Start      (AttrValue<usize>),
	Stop       (AttrValue<usize>),
	Step       (AttrValue<usize>),
	// radio button
	Channel    (AttrValue<Identifier>),
	// number
	Min        (AttrValue<f64>),
	Max        (AttrValue<f64>),
	Grain      (AttrValue<f64>),
}

#[derive(Clone)]
pub enum Attr {
	// component use
	Parameter(String, ActualAttr),
	// any other case
	ActualAttr(ActualAttr),
}

#[derive(Debug)]
pub struct AttrList<'a>(Vec<&'a[Attr]>);

fn parse_variable(k: &str, v: &str) -> Option<ActualAttr> {
	let v = v[1..].into();
	// todo: check valid xml identifier
	let attr = match k {
		"pad"         => ActualAttr::Pad        (Variable(v)),
		"box"         => ActualAttr::Box        (Variable(v)),
		"max"         => ActualAttr::Max        (Variable(v)),
		"min"         => ActualAttr::Min        (Variable(v)),
		"stop"        => ActualAttr::Stop       (Variable(v)),
		"name"        => ActualAttr::Name       (Variable(v)),
		"grow"        => ActualAttr::Grow       (Variable(v)),
		"step"        => ActualAttr::Step       (Variable(v)),
		"txt"         => ActualAttr::Content    (Variable(v)),
		"len"         => ActualAttr::Length     (Variable(v)),
		"start"       => ActualAttr::Start      (Variable(v)),
		"grain"       => ActualAttr::Grain      (Variable(v)),
		"style"       => ActualAttr::Style      (Variable(v)),
		"hover"       => ActualAttr::Hover      (Variable(v)),
		"source"      => ActualAttr::Source     (Variable(v)),
		"divider"     => ActualAttr::Divider    (Variable(v)),
		"channel"     => ActualAttr::Channel    (Variable(v)),
		"overrun"     => ActualAttr::Overrun    (Variable(v)),
		"factory"     => ActualAttr::Factory    (Variable(v)),
		"overflow"    => ActualAttr::Overflow   (Variable(v)),
		"destination" => ActualAttr::Destination(Variable(v)),
		_ => None?
	};
	Some(attr)
}

fn parse_constant(k: &str, v: &str) -> Option<ActualAttr> {
	if v == "default" {
		return match k {
			// component gen
			"factory"     => None?,
			// visible
			"grow"        => Some(ActualAttr::Default),
			"pad"         => Some(ActualAttr::Default),
			"box"         => Some(ActualAttr::Default),
			"style"       => Some(ActualAttr::Default),
			"hover"       => Some(ActualAttr::Default),
			// container
			"len"         => Some(ActualAttr::Default),
			"divider"     => Some(ActualAttr::Default),
			"overrun"     => Some(ActualAttr::Default),
			"overflow"    => Some(ActualAttr::Default),
			// text, label & link
			"txt"         => None?,
			// image & media
			"src"         => None?,
			// link
			"dst"         => None?,
			// checkbox, date
			"name"        => None?,
			// date
			"start"       => None?,
			"stop"        => None?,
			// radio button
			"channel"     => None?,
			// number
			"min"         => None?,
			"max"         => None?,
			"grain"       => None?,
			// bad attribute
			_ => None?
		}
	}
	let attr = match k {
		// component gen
		"factory"     => ActualAttr::Factory    (Constant(Callback::parse(v)?)),
		// visible
		"grow"        => ActualAttr::Grow       (Constant(Grow::parse(v)?)),
		"pad"         => ActualAttr::Pad        (Constant(Length::parse(v)?)),
		"box"         => ActualAttr::Box        (Constant(Length::parse(v)?)),
		"style"       => ActualAttr::Style      (Constant(Identifier::parse(v)?)),
		"hover"       => ActualAttr::Hover      (Constant(Identifier::parse(v)?)),
		// container
		"len"         => ActualAttr::Length     (Constant(Length::parse(v)?)),
		"divider"     => ActualAttr::Divider    (Constant(Length::parse(v)?)),
		"overrun"     => ActualAttr::Overrun    (Constant(Callback::parse(v)?)),
		"overflow"    => ActualAttr::Overflow   (Constant(Callback::parse(v)?)),
		// text, label & link
		"txt"         => ActualAttr::Content    (Constant(v.into())),
		// image & media
		"src"         => ActualAttr::Source     (Constant(v.into())),
		// link
		"dst"         => ActualAttr::Destination(Constant(v.into())),
		// checkbox, date
		"name"        => ActualAttr::Name       (Constant(Identifier::parse(v)?)),
		// date
		"start"       => ActualAttr::Start      (Constant(v.parse::<usize>().ok()?)),
		"stop"        => ActualAttr::Stop       (Constant(v.parse::<usize>().ok()?)),
		// radio button
		"channel"     => ActualAttr::Channel    (Constant(Identifier::parse(v)?)),
		// number
		"min"         => ActualAttr::Min        (Constant(v.parse::<f64>().ok()?)),
		"max"         => ActualAttr::Max        (Constant(v.parse::<f64>().ok()?)),
		"grain"       => ActualAttr::Grain      (Constant(v.parse::<f64>().ok()?)),
		// bad attribute
		_ => None?
	};
	Some(attr)
}

impl ActualAttr {
	pub fn parse(k: &str, v: &str) -> Option<Self> {
		if v.starts_with(":") {
			parse_variable(k, v)
		} else {
			parse_constant(k, v)
		}
	}

	pub fn variant(&self) -> AttrVariant {
		match self {
			Self::Default        => AttrVariant::Default,
			Self::Factory    (_) => AttrVariant::Factory,
			Self::Grow       (_) => AttrVariant::Grow,
			Self::Pad        (_) => AttrVariant::Pad,
			Self::Box        (_) => AttrVariant::Box,
			Self::Style      (_) => AttrVariant::Style,
			Self::Hover      (_) => AttrVariant::Hover,
			Self::Length     (_) => AttrVariant::Length,
			Self::Divider    (_) => AttrVariant::Divider,
			Self::Overrun    (_) => AttrVariant::Overrun,
			Self::Overflow   (_) => AttrVariant::Overflow,
			Self::Content    (_) => AttrVariant::Content,
			Self::Source     (_) => AttrVariant::Source,
			Self::Destination(_) => AttrVariant::Destination,
			Self::Name       (_) => AttrVariant::Name,
			Self::Start      (_) => AttrVariant::Start,
			Self::Stop       (_) => AttrVariant::Stop,
			Self::Step       (_) => AttrVariant::Step,
			Self::Channel    (_) => AttrVariant::Channel,
			Self::Min        (_) => AttrVariant::Min,
			Self::Max        (_) => AttrVariant::Max,
			Self::Grain      (_) => AttrVariant::Grain,
		}
	}

	pub fn variable(&self) -> Option<&str> {
		match self {
			Self::Factory    (Variable(s)) |
			Self::Grow       (Variable(s)) |
			Self::Pad        (Variable(s)) |
			Self::Box        (Variable(s)) |
			Self::Style      (Variable(s)) |
			Self::Hover      (Variable(s)) |
			Self::Length     (Variable(s)) |
			Self::Divider    (Variable(s)) |
			Self::Overrun    (Variable(s)) |
			Self::Overflow   (Variable(s)) |
			Self::Content    (Variable(s)) |
			Self::Source     (Variable(s)) |
			Self::Destination(Variable(s)) |
			Self::Name       (Variable(s)) |
			Self::Start      (Variable(s)) |
			Self::Stop       (Variable(s)) |
			Self::Step       (Variable(s)) |
			Self::Channel    (Variable(s)) |
			Self::Min        (Variable(s)) |
			Self::Max        (Variable(s)) |
			Self::Grain      (Variable(s)) => Some(&s),
			_ => None,
		}
	}
}

impl Attr {
	pub fn parse(p: &str, k: &str, v: &str) -> Option<Self> {
		if p.is_empty() {
			Some(Self::ActualAttr(ActualAttr::parse(k, v)?))
		} else {
			Some(Self::Parameter(p.into(), ActualAttr::parse(k, v)?))
		}
	}

	pub fn variant(&self) -> AttrVariant {
		match self {
			Self::Parameter(_, a) => a.variant(),
			Self::ActualAttr(a) => a.variant(),
		}
	}
}

impl AttrVariant {
	pub fn as_xml_attr(&self) -> &'static str {
		match self {
			Self::Default     => "",
			Self::Factory     => "factory",
			Self::Grow        => "grow",
			Self::Pad         => "pad",
			Self::Box         => "box",
			Self::Style       => "style",
			Self::Hover       => "hover",
			Self::Length      => "len",
			Self::Divider     => "divider",
			Self::Overrun     => "overrun",
			Self::Overflow    => "overflow",
			Self::Content     => "txt",
			Self::Source      => "src",
			Self::Destination => "dst",
			Self::Name        => "name",
			Self::Start       => "start",
			Self::Stop        => "stop",
			Self::Step        => "step",
			Self::Channel     => "channel",
			Self::Min         => "min",
			Self::Max         => "max",
			Self::Grain       => "grain",
		}
	}

	pub fn as_rust_ident(&self) -> &'static str {
		match self {
			Self::Default     => "",
			Self::Factory     => "factory",
			Self::Grow        => "grow",
			Self::Pad         => "pad",
			Self::Box         => "_box",
			Self::Style       => "style",
			Self::Hover       => "hover",
			Self::Length      => "len",
			Self::Divider     => "divider",
			Self::Overrun     => "overrun",
			Self::Overflow    => "overflow",
			Self::Content     => "txt",
			Self::Source      => "src",
			Self::Destination => "dst",
			Self::Name        => "name",
			Self::Start       => "start",
			Self::Stop        => "stop",
			Self::Step        => "step",
			Self::Channel     => "channel",
			Self::Min         => "min",
			Self::Max         => "max",
			Self::Grain       => "grain",
		}
	}

	pub fn as_field_type(&self) -> (&'static str, bool) {
		match self {
			Self::Default     => ("", false),
			Self::Factory     => ("/* not impl */", false),
			Self::Grow        => ("usize", false),
			Self::Pad         => ("usize", false),
			Self::Box         => ("usize", false),
			Self::Style       => ("/* not impl */", false),
			Self::Hover       => ("/* not impl */", false),
			Self::Length      => ("usize", false),
			Self::Divider     => ("/* not impl */", false),
			Self::Overrun     => ("/* not impl */", false),
			Self::Overflow    => ("/* not impl */", false),
			Self::Content     => ("&'a str", true),
			Self::Source      => ("&'a str", true),
			Self::Destination => ("&'a str", true),
			Self::Name        => ("/* not impl */", false),
			Self::Start       => ("/* not impl */", false),
			Self::Stop        => ("/* not impl */", false),
			Self::Step        => ("/* not impl */", false),
			Self::Channel     => ("/* not impl */", false),
			Self::Min         => ("/* not impl */", false),
			Self::Max         => ("/* not impl */", false),
			Self::Grain       => ("/* not impl */", false),
		}
	}
}

impl fmt::Display for ActualAttr {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Self::Default        => write!(f, ""),
			Self::Factory(a)     => a.fmt(f),
			Self::Grow(a)        => a.fmt(f),
			Self::Pad(a)         => a.fmt(f),
			Self::Box(a)         => a.fmt(f),
			Self::Style(a)       => a.fmt(f),
			Self::Hover(a)       => a.fmt(f),
			Self::Length(a)      => a.fmt(f),
			Self::Divider(a)     => a.fmt(f),
			Self::Overrun(a)     => a.fmt(f),
			Self::Overflow(a)    => a.fmt(f),
			Self::Content(a)     => a.fmt(f),
			Self::Source(a)      => a.fmt(f),
			Self::Destination(a) => a.fmt(f),
			Self::Name(a)        => a.fmt(f),
			Self::Start(a)       => a.fmt(f),
			Self::Stop(a)        => a.fmt(f),
			Self::Step(a)        => a.fmt(f),
			Self::Channel(a)     => a.fmt(f),
			Self::Min(a)         => a.fmt(f),
			Self::Max(a)         => a.fmt(f),
			Self::Grain(a)       => a.fmt(f),
		}
	}
}

impl fmt::Debug for Attr {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Self::Parameter(n, a) => write!(f, "use_instance.{} = {}", n, a),
			Self::ActualAttr(a) => {
				if let ActualAttr::Default = a {
					write!(f, "")
				} else {
					let x = a.variant().as_xml_attr();
					write!(f, "{} = {}", x, a)
				}
			},
		}
	}
}

impl fmt::Display for Attr {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", match self {
			Self::Parameter(_, a) => a,
			Self::ActualAttr(a) => a,
		})
	}
}

impl<'a> AttrList<'a> {
	pub fn new(slices: Vec<&'a [Attr]>) -> Self {
		AttrList(slices)
	}

	pub fn to_instantiation(&self) -> String {
		let mut code = String::from("{");
		for slice in &self.0 {
			for attr in *slice {
				let (field, actual_attr) = match attr {
					Attr::Parameter(f, a) => (f.as_str(), a),
					Attr::ActualAttr(a) => {
						if let ActualAttr::Default = a {
							continue;
						}
						(a.variant().as_rust_ident(), a)
					},
				};
				code += &format!("{}:{},", field, actual_attr);
			}
		}
		code + "..Default::default()}"
	}

	pub fn to_declaration(&self) -> (String, bool) {
		let mut code = String::new();
		let mut need_lifetime = false;
		for slice in &self.0 {
			for attr in *slice {
				if let Attr::ActualAttr(a) = attr {
					if let Some(param_name) = a.variable() {
						let (field_type, need_lifetime_2) = a.variant().as_field_type();
						code += &format!("pub {}:{},", param_name, field_type);
						if need_lifetime_2 && !need_lifetime {
							need_lifetime = true;
						}
					}
				}
			}
		}
		(code, need_lifetime)
	}

	pub fn get(&self, variant: AttrVariant) -> Option<&Attr> {
		for slice in &self.0 {
			for attr in *slice {
				if attr.variant() == variant {
					return Some(attr);
				}
			}
		}
		None
	}

	pub fn has(&self, variant: AttrVariant) -> bool {
		for slice in &self.0 {
			for attr in *slice {
				if attr.variant() == variant {
					return true;
				}
			}
		}
		false
	}

	pub fn code<T>(&self, variant: AttrVariant, default: T) -> String
	where T: ToString {
		if let Some(attr) = self.get(variant) {
			attr.to_string()
		} else {
			default.to_string()
		}
	}

	pub fn foreach<F>(&self, mut callback: F)
	where F: FnMut(&Attr) {
		for slice in &self.0 {
			for attr in *slice {
				callback(attr);
			}
		}
	}
}

impl fmt::Display for AttrValue<Callback> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Constant(c) => write!(f, "callbacks.{}", c),
			Variable(s) => write!(f, "instance.{}", s),
		}
	}
}

impl fmt::Display for AttrValue<Identifier> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Constant(c) => write!(f, "{}", c),
			Variable(s) => write!(f, "instance.{}", s),
		}
	}
}

impl fmt::Display for AttrValue<Grow> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Constant(g) => write!(f, "{}", g),
			Variable(s) => write!(f, "instance.{}", s),
		}
	}
}

impl fmt::Display for AttrValue<Length> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Constant(l) => write!(f, "{}", l),
			Variable(s) => write!(f, "instance.{}", s),
		}
	}
}

impl fmt::Display for AttrValue<String> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Constant(s) => write!(f, "{:?}", s),
			Variable(s) => write!(f, "instance.{}", s),
		}
	}
}

impl fmt::Display for AttrValue<usize> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Constant(s) => write!(f, "{}", s),
			Variable(s) => write!(f, "instance.{}", s),
		}
	}
}

impl fmt::Display for AttrValue<f64> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Constant(s) => write!(f, "{:?}", s),
			Variable(s) => write!(f, "instance.{}", s),
		}
	}
}

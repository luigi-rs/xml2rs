use std::cmp::Ordering;
use xmlparser::Tokenizer;
use xmlparser::Token;
use xmlparser::ElementEnd;
use crate::element::Element;
use crate::element::Component;
use crate::element::Attr;
use crate::element::AttrList;

#[derive(Debug)]
pub enum Item {
	Element(Element),
	CustomDeclare(String, Element),
	CustomExtends(String, usize),
	CustomInstance(usize),
}

#[derive(Debug)]
pub struct Node {
	pub depth: usize,
	pub item: Item,
	pub attributes: Vec<Attr>,
}

#[derive(Debug)]
pub struct Tree {
	elements: Vec<Node>,
}

impl Tree {
	pub fn new() -> Self {
		Tree {
			elements: Vec::new(),
			// iter_idx: 0,
		}
	}

	pub fn renderables(&self) -> Vec<usize> {
		let mut indexes = Vec::new();
		for i in 0..self.elements.len() {
			let node = &self.elements[i];
			if node.is_renderable() {
				indexes.push(i);
			}
		}
		indexes
	}

	pub fn root(&self) -> Option<usize> {
		for i in 0..self.elements.len() {
			let node = &self.elements[i];
			if node.is_renderable() {
				return Some(i);
			}
		}
		None
	}

	pub fn children(&self, i: usize) -> Vec<usize> {
		let mut indexes = Vec::new();
		let c_depth = self.elements[i].depth + 1;
		for i in (i+1)..self.elements.len() {
			let node = &self.elements[i];
			match node.depth.cmp(&c_depth) {
				Ordering::Less => break,
				Ordering::Equal if node.is_renderable() => indexes.push(i),
				_ => (),
			}
		}
		indexes
	}

	fn find_custom(&self, key: &str, mut max_depth: usize) -> Option<usize> {
		for i in (0..self.elements.len()).rev() {
			let e = &self.elements[i];
			let depth = e.depth;
			if depth <= max_depth {
				max_depth = depth;
				let found = match &e.item {
					Item::CustomDeclare(s, _) if s == key => true,
					Item::CustomExtends(s, _) if s == key => true,
					_ => false,
				};
				if found {
					return Some(i);
				}
			}
		}
		None
	}

	pub fn get(&self, index: usize) -> (&Element, AttrList, usize) {
		let node = &self.elements[index];
		match &node.item {
			Item::Element(e) => (e, AttrList::new(vec![&node.attributes]), node.depth),
			Item::CustomInstance(i) => {
				let mut attributes = vec![node.attributes.as_slice()];
				let mut i = *i;
				let e = loop {
					let other_node = &self.elements[i];
					attributes.push(other_node.attributes.as_slice());
					match &other_node.item {
						Item::CustomDeclare(_, e) => break e,
						Item::CustomExtends(_, new_i) => i = *new_i,
						_ => unreachable!(),
					}
				};
				(&e, AttrList::new(attributes), node.depth)
			},
			_ => panic!("element at specified index is not renderable"),
		}
	}

	fn check_attributes(&mut self, len_bck: usize) -> Result<(), String> {
		// todo: check illegal element nesting
		let mut err_msgs = Vec::new();
		for i in self.renderables() {
			let (e, attributes, _) = self.get(i);
			let (missing, invalid) = e.check_attrs(&attributes);
			if !missing.is_empty() {
				err_msgs.push(format!("E{}: Missing attributes: {}", i, missing.join(", ")));
			}
			if !invalid.is_empty() {
				err_msgs.push(format!("E{}: Invalid attributes: {}", i, invalid.join(", ")));
			}
		}
		if !err_msgs.is_empty() {
			self.elements.truncate(len_bck);
			Err(err_msgs.join("\n"))
		} else {
			Ok(())
		}
	}

	pub fn add_from_xml(&mut self, xml: &str) -> Result<(), String> {
		let len_bck = self.elements.len();
		let mut cursor = None;
		let mut depth = 0;
		for token in Tokenizer::from(xml) {
			match token.unwrap() {
				Token::ElementStart { prefix, local, .. } => {
					if let Some(_) = cursor {
						break;
					}
					let item = match (prefix.as_str(), self.find_custom(&local, depth)) {
						("", Some(i)) => Some(Item::CustomInstance(i)),
						_ => match Element::parse(&prefix, &local) {
							Some(e) => Some(Item::Element(e)),
							_ => match self.find_custom(&prefix, depth) {
								Some(i) => Some(Item::CustomExtends((&local).to_string(), i)),
								_ => match Element::parse("", &prefix) {
									Some(e) => Some(Item::CustomDeclare((&local).to_string(), e)),
									_ => None,
								},
							},
						}
					};
					if let Some(item) = item {
						cursor = Some(Node { depth, item, attributes: vec![] });
					} else {
						let idx = self.elements.len();
						self.elements.truncate(len_bck);
						Err(format!("Bad element {:?} (@{})", local.as_str(), idx))?;
					}
				},
				Token::Attribute { prefix, local, value, .. } => {
					let e = cursor.as_mut().unwrap();
					if prefix.is_empty() != e.is_component_use() {
						let attr = Attr::parse(&prefix, &local, &value);
						if let Some(attr) = attr {
							e.attributes.push(attr);
						} else {
							let idx = self.elements.len();
							self.elements.truncate(len_bck);
							Err(format!("Bad attribute {:?} ({:?}@{})", local, e.item, idx))?;
						}
					} else {
						let idx = self.elements.len();
						self.elements.truncate(len_bck);
						Err(format!("Invalid attribute prefix {:?} ({:?}@{})", prefix, e.item, idx))?;
					}
				},
				Token::ElementEnd { end, .. } => match end {
					ElementEnd::Open |
					ElementEnd::Empty => {
						let mut new_cursor = None;
						std::mem::swap(&mut new_cursor, &mut cursor);
						self.elements.push(new_cursor.unwrap());
						if let ElementEnd::Open = end {
							depth += 1;
						}
					},
					ElementEnd::Close(..) => depth -= 1,
				},
				_ => {},
			}
		}
		if cursor.is_some() || depth != 0 {
			self.elements.truncate(len_bck);
			Err("Bad xml nesting".into())
		} else {
			self.check_attributes(len_bck)
		}
	}
}

impl Node {
	pub fn is_component_use(&self) -> bool {
		match self {
			Node {
				item: Item::Element(
					Element::Component(
						Component::Use(_)
					)
				),
				..
			} => true,
			_ => false,
		}
	}

	pub fn is_renderable(&self) -> bool {
		match &self.item {
			Item::Element(_) |
			Item::CustomInstance(_) => true,
			_ => false
		}
	}
}

use crate::element::AttrVariant;

pub const SUPPORTS: [AttrVariant; 5] = [
	AttrVariant::Grow,
	AttrVariant::Pad,
	AttrVariant::Box,
	AttrVariant::Style,
	AttrVariant::Hover,
];

pub const REQUIRES: [AttrVariant; 1] = [
	AttrVariant::Name,
];